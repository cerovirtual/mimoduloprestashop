<h2>{l s="Fotos subidas" mod="mimoduloprestashop"}</h2>
<div class="col-xs-12 col-md-12" id="product-photo-wrapper">
    {foreach from=$photos item=photo}
    <div id="product-photo-comment">
        {if $enable_comment == '1'}
            <p>{$photo.comment}</p>
        {/if}
        <img class="fotocliente_img" src="{$base_dir}{$photo.foto}">
    </div>
    <br>
    <br>
    {/foreach}
</div>