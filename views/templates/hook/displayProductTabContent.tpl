<h3 class="page-product-heading">{l s="Fotos de Clientes" mod="mimoduloprestashop"}</h3>
<h4><strong>{l s="Subir una foto" mod="mimoduloprestashop"}</strong></h4>
{if isset($savedForm)}
    <div class="alert alert-success">{$savedForm}</div>
{/if}
{if isset($errorForm)}
    <div class="alert alert-danger">{$errorForm}</div>
{/if}
<div class="mimoduloprestashop_bloque">
    <form action="" enctype="multipart/form-data" method="post" id="comment-form">
        <div class="form-group  col-xs-12 col-md-4">
            <label for="photo">{l s="Foto" mod="mimoduloprestashop"}:</label>
            <input type="file" name="photo" id="photo">
        </div>
        <div class="form-group col-xs-12 col-md-8" style="{if '0' == $enable_comment }display: none{/if}">
            <label for="comment">{l s="Comentario" mod="mimoduloprestashop"}:</label>
            <textarea name="comment" id="comment" class="formcontrol"></textarea>
        </div>
        <br>
        <div class="submit mimoduloprestashop_bloque">
            <button type="submit" name="mimoduloprestashop_submit_photo" class="button btn btn-default button-medium">
                <span>{l s="Enviar" mod="mimoduloprestashop"}<i class="icon-chevron-right right"> </i></span>
            </button>
        </div>
    </form>
</div>
<br>
<h4>{l s="Fotos subidas" mod="mimoduloprestashop"}</h4>
<div class="col-xs-12 col-md-12" id="product-photo-wrapper">
    {assign var=count value=1}
    {foreach from=$productPhotos item=productPhoto}
        <div id="product-photo-comment">
            {if $enable_comment == '1'}
                <div class="col-xs-12 col-md-12" id="product-comment">
                    <p>{$productPhoto.comment}</p>
                </div>
            {/if}
            <div class="col-xs-12 col-md-12" id="product-photo">
                <img class="fotocliente_img col-xs-6 col-md-6" src="{$base_dir}{$productPhoto.foto}">
                <span class="col-xs-6 col-md-6">&nbsp;</span>
            </div>
        </div>
        <br>
        {* Only show 3 photos *}
        {$count = $count + 1}
        {if $count > 3}
            {break}
        {/if}
    {/foreach}
</div>
<div>
    {* Link to all photos page *}
    {assign var=parms value=['module_action' => 'listphotos']}
    <a href="{$link->getModuleLink('mimoduloprestashop', 'fotos', $parms)}"><h3>{l s="Ver más fotos ..." mod="mimoduloprestashop"}</a></h3>
</div>