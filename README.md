**This is a test module for the prestashop ecommerce system**

**Installation of Prestashop Example:**
    
`php install-dev/index_cli.php --language=es --timezone=America/Mexico_City --domain=prestashop.dev --db_server=localhost --db_user=root --db_password=root --db_name=prestashop --name=TiendaCV --country=mx --firstname=Miguel --lastname=Villafuerte --password=admin --email=mikeshot@gmail.com`


**Installation of Hotel Commerce Example:**
    
`php install/index_cli.php --language=es --timezone=America/Mexico_City --domain=prestashop.commerce.dev --db_server=localhost --db_user=root --db_password=root --db_name=prestashop_commerce --name=HotelCV --country=mx --firstname=Miguel --lastname=Villafuerte --password=admin --email=mikeshot@gmail.com --send_email=0`