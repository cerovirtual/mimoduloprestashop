<?php

class MimoduloprestashopFotosModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();

        switch (Tools::getValue('module_action')) {
            case 'listphotos':
                $this->listPhotos();
                break;

            default:
                return true;
        }
    }

    /*
     * Controller route:
     * mystore/index.php?fc=module&module=MY_MODULE&controller=CONTROLLER&VAR_NAME=VAR_VALUE
     *
     * mystore/index.php?fc=module&module=mimoduloprestashop&controller=fotos&module_action=listphotos
     */
    protected function listPhotos()
    {
        // Loads config values
        $enable_comment = Configuration::get('MIMODULOPRESTASHOP_COMMENT');

        // Get all photos
        $photos = FotoclienteObj::getProductPhotosAll();

        // Assign values to template
        $this->context->smarty->assign('enable_comment', $enable_comment);
        $this->context->smarty->assign('photos', $photos);

        //parent::setMedia();
        $this->addJS(_MODULE_DIR_ . 'mimoduloprestashop/views/js/mimoduloprestashop.js');
        $this->addCSS(_MODULE_DIR_ . 'mimoduloprestashop/views/css/mimoduloprestashop.css');

        // Assign template
        $this->setTemplate('photoList.tpl');
    }
}