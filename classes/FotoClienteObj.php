<?php

class FotoclienteObj extends ObjectModel
{
    public $id_foto;

    public $id_product;

    public $foto;

    public $comment;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'fotocliente_item',
        'primary'   => 'id_foto',
        'multilang' => false,
        'fields'    => array(
            'id_product' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedId',
                'required' => true
            ),
            'foto' => array(
                'type' => self::TYPE_STRING,
                'required' => true
            ),
            'comment' => array(
                'type' => self::TYPE_HTML,
                'validate' => 'isCleanHtml'
            ),
        ),
    );

    public static function getProductPhotosById($id_product)
    {
        return Db::getInstance()->executeS("
            SELECT * FROM `"._DB_PREFIX_."fotocliente_item`
            WHERE `id_product` = ".(int)$id_product."
            ORDER BY `id_foto` DESC
        ");
    }

    public static function getProductPhotosAll()
    {
        return Db::getInstance()->executeS("
            SELECT * FROM `"._DB_PREFIX_."fotocliente_item`
            ORDER BY `id_foto` DESC
        ");
    }
}
