<?php

require_once(dirname(__FILE__) . '/classes/FotoClienteObj.php');

class mimoduloprestashop extends Module
{
    protected $photoFormError;

    protected $saveImgPath = 'upload';

    protected $imgName;

    function __construct()
    {
        // Module info
        $this->name                   = 'mimoduloprestashop';
        $this->displayName            = $this->l('Mi Módulo de prueba');
        $this->description            = $this->l('Módulo de prueba para desarrollar en el sistema Prestashop');
        $this->tab                    = 'front_office_features';
        $this->author                 = 'Cero Virtual';
        $this->version                = '0.1';
        $this->bootstrap              = true;
        $this->ps_versions_compliancy = array('min' => '1.5.2', 'max' => '1.7.2');
        // $this->dependencies           = array('blockbanner');

        parent::__construct();
    }

    public function getContent()
    {
        // If config form submitted, saves it
        if (Tools::isSubmit('mimoduloprestashop_form')) {
            $enable_comment = Tools::getValue('enable_comment');
            Configuration::updateValue('MIMODULOPRESTASHOP_COMMENT', $enable_comment);
        }

        // Loads config values
        $enable_comment = Configuration::get('MIMODULOPRESTASHOP_COMMENT');

        // Assign config values to template
        $this->context->smarty->assign('enable_comment', $enable_comment);

        // Display view
        return $this->display(__FILE__, 'getContent.tpl');
    }

    public function install()
    {
        // Call parent install method
        if ( !parent::install()) {
            return false;
        }

        // Stores initial config values
        Configuration::updateValue('MIMODULOPRESTASHOP_COMMENT', '1');

        // Register hooks
        $this->registerHook('displayProductTabContent');

        // Setup database
        return $this->setupDB();
    }

    public function uninstall()
    {
        // Call parent uninstall method
        if ( !parent::uninstall()) {
            return false;
        }

        // Remove config entries
        Configuration::deleteByName('MIMODULOPRESTASHOP_COMMENT');

        // Clean up database
        return $this->cleanupDB();
    }

    public function onClickOption($type, $href = false)
    {
        $matchType = array(
            'reset' => "return confirm('" . $this->l('Está seguro de resetear el módulo?') . "');",
            'delete' => "return confirm('" . $this->l('Está seguro de eliminar el módulo?') . "');",
        );

        if (isset($matchType[$type])) {
            return $matchType[$type];
        }

        return '';
    }

    public function setupDB()
    {
        return Db::getInstance()->execute("
            CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."fotocliente_item` (
              `id_foto` int(11) NOT NULL AUTO_INCREMENT,
              `id_product` int(11) NOT NULL,
              `foto` VARCHAR( 255 ) NOT NULL,
              `comment` text NOT NULL,
              PRIMARY KEY (`id_foto`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
	    ");
    }

    public function cleanupDB(){
        return Db::getInstance()->execute("
            DROP TABLE `"._DB_PREFIX_."fotocliente_item`;
        ");
    }

    public function HookDisplayProductTabContent($params)
    {
        // If photo form submitted, process it
        if (Tools::isSubmit('mimoduloprestashop_submit_photo') && isset($_FILES['photo'])) {
            if ( !$this->processPhotoForm($_FILES['photo'])) {
                $this->context->smarty->assign('errorForm', $this->photoFormError);
            }

            // Saves the image & comment
            $photoClient = new FotoclienteObj();
            $photoClient->id_product = pSQL(Tools::getValue('id_product'));
            $photoClient->foto = pSQL($this->imgName);
            $photoClient->comment = pSQL(Tools::getValue('comment'));
            if ( !$photoClient->add()) {
                $this->context->smarty->assign('errorForm', $this->l('No se ha podido guardar la información.'));
            }

            $this->context->smarty->assign('savedForm', $this->l('Imagen añadida.'));
        }

        // Gets product photos & comments, if any
        $productPhotos = FotoclienteObj::getProductPhotosById(Tools::getValue('id_product'));

        // Read module config
        $enable_comment = Configuration::get('MIMODULOPRESTASHOP_COMMENT');

        // Assign config value to template
        $this->context->smarty->assign('enable_comment', $enable_comment);
        $this->context->smarty->assign('productPhotos', $productPhotos);

        // Assign custom CSS & JS
        $this->context->controller->addCSS($this->_path . 'views/css/mimoduloprestashop.css');
        $this->context->controller->addJS($this->_path . 'views/js/mimoduloprestashop.js');

        // Display template
        return $this->display(__FILE__, 'displayProductTabContent.tpl');
    }

    public function processPhotoForm($photo)
    {
        $this->photoFormError = '';
        $allowedTypes = array('image/gif', 'image/jpg', 'image/jpeg', 'image/png',);

        // Validates photo
        if ( !in_array($photo['type'], $allowedTypes)) {
            $this->photoFormError = $this->l('Tipo de imagen no soportado.');
            return false;
        }

        if ( !is_file($photo['tmp_name'])) {
            $this->photoFormError = $this->l('Nombre inválido de imagen temporal.');
            return false;
        }

        // Get image properties
        list($width, $height) = getimagesize($photo['tmp_name']);

        // Calculate proportion of (400 x n)
        $newHeight = (400 / $width) * $height;

        // Copies the image to it's final destination
        $this->imgName = sprintf("%s/%s_%s", $this->saveImgPath, date('YmdHisu'), $photo['name']);

        if ( !ImageManager::resize(
            $photo['tmp_name'], $this->imgName, 400, $newHeight, $photo['type']
        )) {
            $this->photoFormError = $this->l('No se pudo guardar la imagen.');
            return false;
        }

        return true;
    }
}