<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{mimoduloprestashop}prestashop>mimoduloprestashop_8bfd90429f4cefcf813e28b67ba2381f'] = 'My Test module';
$_MODULE['<{mimoduloprestashop}prestashop>mimoduloprestashop_8033260f08f32a65b51feb172ebc3128'] = 'Test module to develop on the Prestashop system';
$_MODULE['<{mimoduloprestashop}prestashop>mimoduloprestashop_fa1b92a8add577e593265b8e5e5b5274'] = 'Are you sure to reset the module?';
$_MODULE['<{mimoduloprestashop}prestashop>mimoduloprestashop_6e7765264157db46d4272862c7a5cd84'] = 'Are you sure to uninstall the module?';
$_MODULE['<{mimoduloprestashop}prestashop>mimoduloprestashop_98db3e4822df1a4f0bee64eaf40c61e9'] = 'The information can not be saved.';
$_MODULE['<{mimoduloprestashop}prestashop>mimoduloprestashop_714a2fdee7a31488858dcf1dcb3afbaa'] = 'Image added.';
$_MODULE['<{mimoduloprestashop}prestashop>mimoduloprestashop_0930bbb66fc89e4c0201432bf94ba253'] = 'Image type unsupported.';
$_MODULE['<{mimoduloprestashop}prestashop>mimoduloprestashop_81e6b01aa6e260ddcffe3490e19522c0'] = 'Invalid temp file name.';
$_MODULE['<{mimoduloprestashop}prestashop>mimoduloprestashop_a2d31694145250365c1b318707faff7b'] = 'The image can not be saved.';
